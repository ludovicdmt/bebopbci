import numpy as np
import matplotlib.pyplot as plt
import pickle
from os.path import join
from matplotlib.style import use

from moabb.datasets import (
    Zhou2016,
    BNCI2014004,
    BNCI2014001,
    MunichMI,
    Weibo2014,
    Cho2017,
)

from preprocess import (
    load_sub,
    apply_preprocessing,
    Dreyer2023,
)
from help_funcs import load_exp_variables, vis_permutation_cluster_test

use("default")


# ----- #
# Hyperparameters.
metric = "rocauc"
if metric == "rocauc":
    metric_str = "score"
elif metric == "accuracy":
    metric_str = metric

remove_fooof = False
if remove_fooof == True:
    fooof_save_str = ""
elif remove_fooof == False:
    fooof_save_str = "_nfs"

clm_str_tr = "_tr"
clm_str_sl = "_sl"

savefigs = True  # True, False

perm = True  # True, False

plot_format = "png"  # "pdf", "png"


# ----- #
# Dataset selection.
datas = [
    "zhou2016",
    "2014004",
    "2014001",
    "weibo2014",
    # "munichmi",
    # "cho2017",
    # "dreyer2023",
]

# Mode.
mode = "local"  # "local", "cluster"
if mode == "local":
    basepath = "/home/sotpapad/Codes/"
elif mode == "cluster":
    basepath = "/mnt/data/sotiris.papadopoulos/"


# ----- #
# Results selestion.
classification_mode = "both"   # "time_resolved", "both"
if classification_mode == "time_resolved":
    res_to_vis = [
        "conv",
        "fb_beta",
        # "fb_mu",
        "fb_mubeta",
        "fb_1_beta",
        "fb_1_mubeta",
        "conv_riemann",
    ]
elif classification_mode == "both":
    # HARD CODED ASSUMPTION: sliding window results
    # always come last in the list, and convolution is
    # always the first of either category.
    res_to_vis = [
        "conv",
        "fb_beta",
        # "fb_mu",
        "fb_mubeta",
        "fb_1_beta",
        # "fb_1_mu",
        "fb_1_mubeta",
        "conv_sliding",
        "fb_beta_sliding",
        # "fb_mu_sliding",
        "fb_mubeta_sliding",
        "fb_1_beta_sliding",
        # "fb_1_mu_sliding",
        "fb_1_mubeta_sliding",
    ]

    sl_res_id = int(np.where(np.array(res_to_vis)=="conv_sliding")[0])

filter_banks = [
    [[6, 9], [9, 12], [12, 15]],
    [[15, 18], [18, 21], [21, 24], [24, 27], [27, 30]],
    [[6, 9], [9, 12], [12, 15], [15, 18], [18, 21], [21, 24], [24, 27], [27, 30]],
    [[6, 15]],
    [[15, 30]],
    [[6, 30]],
]

power_band = filter_banks[3][0]


# ----- #
# Figure initialization.
screen_res = [1920, 972]
dpi = 300

linewidths = [1.5, 0.75, 1.0]
fontsizes = [6, 5, 4]
tick_size = 4

if savefigs == False:
    hratios_0 = np.ones(len(datas))
    hratios_1 = np.ones(len(datas) + 1)
    hratios_1[0] = 0.3

    fig0 = plt.figure(
        constrained_layout=False,
        figsize=(screen_res[0] / dpi, screen_res[1] / dpi),
        dpi=dpi,
    )
    gs0 = fig0.add_gridspec(
        nrows=len(datas),
        ncols=4,
        wspace=0.25,
        hspace=0.5,
        left=0.05,
        right=0.95,
        top=0.90,
        bottom=0.05,
        width_ratios=[1.0, 1.0, 1.0, 1.0],
        height_ratios=hratios_0,
    )

    fig1 = plt.figure(
        constrained_layout=False,
        figsize=(screen_res[0] / dpi, screen_res[1] / dpi),
        dpi=dpi,
    )
    gs1 = fig1.add_gridspec(
        nrows=len(datas) + 1,
        ncols=6,
        wspace=0.25,
        hspace=0.25,
        left=0.05,
        right=0.95,
        top=0.95,
        bottom=0.05,
        width_ratios=[1.0, 1.0, 1.0, 1.0, 1.0, 1.0],
        height_ratios=hratios_1,
    )

else:
    hratios_0 = np.ones(len(datas))
    hratios_1 = np.ones(len(datas) + 1)
    hratios_1[0] = 0.3

    fig0 = plt.figure(constrained_layout=False, figsize=(7, 2.5 * len(datas)), dpi=dpi)
    gs0 = fig0.add_gridspec(
        nrows=len(datas),
        ncols=4,
        wspace=0.25,
        hspace=0.3,
        left=0.07,
        right=0.95,
        top=0.90,
        bottom=0.10,
        width_ratios=[1.0, 1.0, 1.0, 1.0],
        height_ratios=hratios_0,
    )

    fig1 = plt.figure(constrained_layout=False, figsize=(7, 1.5 * len(datas)), dpi=dpi)
    gs1 = fig1.add_gridspec(
        nrows=len(datas) + 1,
        ncols=6,
        wspace=0.25,
        hspace=0.30,
        left=0.05,
        right=0.95,
        top=0.90,
        bottom=0.05,
        width_ratios=[1.0, 1.0, 1.0, 1.0, 1.0, 1.0],
        height_ratios=hratios_1,
    )


# ----- #
# Data loading.
snrs_all = []
for d, data in enumerate(datas):
    if data == "zhou2016":
        dataset = Zhou2016()
        variables_path = "{}zhou_2016/variables.json".format(basepath)
        title_str = "Zhou 2016"
        data_color = "cornflowerblue"
        c3c4_ids = [3, 5]
    elif data == "2014004":
        dataset = BNCI2014004()
        variables_path = "{}2014_004/variables.json".format(basepath)
        title_str = "BNCI 2014-004"
        data_color = "orchid"
        c3c4_ids = [0, 2]
    elif data == "2014001":
        dataset = BNCI2014001()
        variables_path = "{}2014_001/variables.json".format(basepath)
        title_str = "BNCI 2014-001"
        data_color = "darkgoldenrod"
        c3c4_ids = [3, 5]
    elif data == "munichmi":
        dataset = MunichMI()
        variables_path = "{}munichmi/variables.json".format(basepath)
        title_str = "Munich MI\n(Grosse-Wentrup 2009)"
        data_color = "orangered"
        c3c4_ids = [4, 8]
    elif data == "cho2017":
        dataset = Cho2017()
        variables_path = "{}cho_2017/variables.json".format(basepath)
        title_str = "Cho 2017"
        data_color = "yellow"
        c3c4_ids = [3, 5]
    elif data == "weibo2014":
        dataset = Weibo2014()
        variables_path = "{}weibo_2014/variables.json".format(basepath)
        title_str = "Weibo 2014"
        data_color = "mediumseagreen"
        c3c4_ids = [3, 5]
    elif data == "dreyer2023":
        dataset = Dreyer2023()
        variables_path = "{}dreyer_2023/variables.json".format(basepath)
        title_str = "Dreyer 2023"
        data_color = "navy"
        c3c4_ids = [3, 5]

    print("Analyzing {} dataset...".format(title_str))

    # ----- #
    # Loading of dataset-specific variables.
    experimental_vars = load_exp_variables(json_filename=variables_path)

    savepath = experimental_vars["dataset_path"]

    subs = np.arange(1, experimental_vars["n_subjects"] + 1, 1).tolist()
    if data == "cho2017":
        # Some subjects are not included in the dataset.
        subs = np.delete(np.array(subs), [31, 45, 48]).tolist()

    exp_time_periods = experimental_vars["exp_time_periods"]
    bin_dt = experimental_vars["bin_dt"]

    # Time in 100ms non-overlapping windows.
    time = np.around(
        np.arange(
            exp_time_periods[1] - 0.5, exp_time_periods[2] + 0.5 + bin_dt, bin_dt
        ),
        2,
    )

    # Time in 1000ms sliding (by 50ms) windows.
    time_sl = np.around(
        np.arange(
            exp_time_periods[1] - 0.5, exp_time_periods[2] + 0.5 + bin_dt / 2, bin_dt / 2
        ),
        3,
    )

    tmin = experimental_vars["tmin"]
    tmax = experimental_vars["tmax"]

    time_points = [0.0, 0.5, 1.0, 2.0, exp_time_periods[2]]
    time_ids = [np.where(time == tp)[0][0] for tp in time_points]
    time_ids_sl = [np.where(time_sl == tp)[0][0] for tp in time_points]

    # Time in experiment.
    sfreq = experimental_vars["sfreq"]
    exp_time = np.linspace(tmin, tmax, int((np.abs(tmin) + np.abs(tmax)) * sfreq) + 1)
    exp_time = np.around(exp_time, decimals=3)
    task_time_lims = [exp_time_periods[1], exp_time_periods[2]]
    base_start = -0.5

    try:
        base_start_id = np.where(exp_time == base_start)[0][0]
    except:
        base_start_id = np.where(exp_time >= base_start)[0][0][0]
    try:
        trial_start_id = np.where(exp_time == task_time_lims[0])[0][0]
    except:
        trial_start_id = np.where(exp_time >= task_time_lims[0])[0][0][0]
    try:
        trial_end_id = np.where(exp_time == task_time_lims[1])[0][0]
    except:
        trial_end_id = np.where(exp_time <= task_time_lims[1])[0][0][-1]


    # ----- #
    # Loading of decoding results.
    # Dataset results.
    results = []
    results_std = []
    perm_data = []
    label_strs = []
    colors = []

    # All convolution results.
    if "conv" in res_to_vis:
        label_strs.append("Convolution & CSP")
        colors.append("orangered")
        results.append(
            np.mean(
                np.load(
                    savepath
                    + "mean_{}_power_beta_band_csp{}_conv_waves{}.npy".format(
                        metric, fooof_save_str, clm_str_tr
                    )
                ),
                axis=(2, 3),
            ),
        )
        results_std.append(
            np.load(
                savepath
                + "std_{}_power_beta_band_csp{}_conv_waves{}.npy".format(
                    metric, fooof_save_str, clm_str_tr
                )
            ),
        )
        perm_data.append(
            np.mean(
                np.load(
                    savepath
                    + "mean_{}_power_beta_band_csp{}_conv_waves{}.npy".format(
                        metric, fooof_save_str, clm_str_tr
                    )
                ),
                axis=-1,
            ),
        )

    # All filter bank results.
    if "fb_beta" in res_to_vis:
        label_strs.append(
            "Filter bank & CSP ({}-{} Hz; {} bands; {} Hz step)".format(
                filter_banks[1][0][0],
                filter_banks[1][-1][1],
                len(filter_banks[1]),
                filter_banks[1][0][1] - filter_banks[1][0][0],
            ),
        )
        colors.append("goldenrod")
        results.append(
            np.mean(
                np.load(
                    savepath
                    + "mean_{}_power_filter_bank_{}_csp_{}_{}{}.npy".format(
                        metric,
                        len(filter_banks[1]),
                        filter_banks[1][0][0],
                        filter_banks[1][-1][1],
                        clm_str_tr,
                    )
                ),
                axis=(2, 3),
            ),
        )
        results_std.append(
            np.load(
                savepath
                + "std_{}_power_filter_bank_{}_csp_{}_{}{}.npy".format(
                    metric,
                    len(filter_banks[1]),
                    filter_banks[1][0][0],
                    filter_banks[1][-1][1],
                    clm_str_tr,
                )
            ),
        )
        perm_data.append(
            np.mean(
                np.load(
                    savepath
                    + "mean_{}_power_filter_bank_{}_csp_{}_{}{}.npy".format(
                        metric,
                        len(filter_banks[1]),
                        filter_banks[1][0][0],
                        filter_banks[1][-1][1],
                        clm_str_tr,
                    )
                ),
                axis=-1,
            ),
        )

    if "fb_mu" in res_to_vis:
        label_strs.append(
            "Filter bank & CSP ({}-{} Hz; {} bands; {} Hz step)".format(
                filter_banks[0][0][0],
                filter_banks[0][-1][1],
                len(filter_banks[0]),
                filter_banks[0][0][1] - filter_banks[0][0][0],
            ),
        )
        colors.append("mediumturquoise")
        results.append(
            np.mean(
                np.load(
                    savepath
                    + "mean_{}_power_filter_bank_{}_csp_{}_{}{}.npy".format(
                        metric,
                        len(filter_banks[0]),
                        filter_banks[0][0][0],
                        filter_banks[0][-1][1],
                        clm_str_tr,
                    )
                ),
                axis=(2, 3),
            ),
        )
        results_std.append(
            np.load(
                savepath
                + "std_{}_power_filter_bank_{}_csp_{}_{}{}.npy".format(
                    metric,
                    len(filter_banks[0]),
                    filter_banks[0][0][0],
                    filter_banks[0][-1][1],
                    clm_str_tr,
                )
            ),
        )
        perm_data.append(
            np.mean(
                np.load(
                    savepath
                    + "mean_{}_power_filter_bank_{}_csp_{}_{}{}.npy".format(
                        metric,
                        len(filter_banks[0]),
                        filter_banks[0][0][0],
                        filter_banks[0][-1][1],
                        clm_str_tr,
                    )
                ),
                axis=-1,
            ),
        )

    if "fb_mubeta" in res_to_vis:
        label_strs.append(
            "Filter bank & CSP ({}-{} Hz; {} bands; {} Hz step)".format(
                filter_banks[2][0][0],
                filter_banks[2][-1][1],
                len(filter_banks[2]),
                filter_banks[2][0][1] - filter_banks[2][0][0],
            ),
        )
        colors.append("mediumorchid")
        results.append(
            np.mean(
                np.load(
                    savepath
                    + "mean_{}_power_filter_bank_{}_csp_{}_{}{}.npy".format(
                        metric,
                        len(filter_banks[2]),
                        filter_banks[2][0][0],
                        filter_banks[2][-1][1],
                        clm_str_tr,
                    )
                ),
                axis=(2, 3),
            ),
        )
        results_std.append(
            np.load(
                savepath
                + "std_{}_power_filter_bank_{}_csp_{}_{}{}.npy".format(
                    metric,
                    len(filter_banks[2]),
                    filter_banks[2][0][0],
                    filter_banks[2][-1][1],
                    clm_str_tr,
                )
            ),
        )
        perm_data.append(
            np.mean(
                np.load(
                    savepath
                    + "mean_{}_power_filter_bank_{}_csp_{}_{}{}.npy".format(
                        metric,
                        len(filter_banks[2]),
                        filter_banks[2][0][0],
                        filter_banks[2][-1][1],
                        clm_str_tr,
                    )
                ),
                axis=-1,
            ),
        )

    # All filtering results.
    if "fb_1_beta" in res_to_vis:
        label_strs.append(
            "Beta band filter & CSP ({}-{} Hz)".format(
                filter_banks[4][0][0],
                filter_banks[4][0][1],
            ),
        )
        colors.append("goldenrod")
        results.append(
            np.mean(
                np.load(
                    savepath
                    + "mean_{}_power_filter_bank_{}_csp_{}_{}{}.npy".format(
                        metric,
                        len(filter_banks[4]),
                        filter_banks[4][0][0],
                        filter_banks[4][-1][1],
                        clm_str_tr,
                    )
                ),
                axis=(2, 3),
            ),
        )
        results_std.append(
            np.load(
                savepath
                + "std_{}_power_filter_bank_{}_csp_{}_{}{}.npy".format(
                    metric,
                    len(filter_banks[4]),
                    filter_banks[4][0][0],
                    filter_banks[4][-1][1],
                    clm_str_tr,
                )
            ),
        )
        perm_data.append(
            np.mean(
                np.load(
                    savepath
                    + "mean_{}_power_filter_bank_{}_csp_{}_{}{}.npy".format(
                        metric,
                        len(filter_banks[4]),
                        filter_banks[4][0][0],
                        filter_banks[4][-1][1],
                        clm_str_tr,
                    )
                ),
                axis=-1,
            ),
        )
    
    if "fb_1_mu" in res_to_vis:
        label_strs.append(
            "Mu band filter & CSP ({}-{} Hz)".format(
                filter_banks[3][0][0],
                filter_banks[3][0][1],
            ),
        )
        colors.append("mediumturquoise")
        results.append(
            np.mean(
                np.load(
                    savepath
                    + "mean_{}_power_filter_bank_{}_csp_{}_{}{}.npy".format(
                        metric,
                        len(filter_banks[3]),
                        filter_banks[3][0][0],
                        filter_banks[3][-1][1],
                        clm_str_tr,
                    )
                ),
                axis=(2, 3),
            ),
        )
        results_std.append(
            np.load(
                savepath
                + "std_{}_power_filter_bank_{}_csp_{}_{}{}.npy".format(
                    metric,
                    len(filter_banks[3]),
                    filter_banks[3][0][0],
                    filter_banks[3][-1][1],
                    clm_str_tr,
                )
            ),
        )
        perm_data.append(
            np.mean(
                np.load(
                    savepath
                    + "mean_{}_power_filter_bank_{}_csp_{}_{}{}.npy".format(
                        metric,
                        len(filter_banks[3]),
                        filter_banks[3][0][0],
                        filter_banks[3][-1][1],
                        clm_str_tr,
                    )
                ),
                axis=-1,
            ),
        )

    if "fb_1_mubeta" in res_to_vis:
        label_strs.append(
            "Mu-beta band filter & CSP ({}-{} Hz)".format(
                filter_banks[5][0][0],
                filter_banks[5][0][1],
            ),
        )
        colors.append("mediumorchid")
        results.append(
            np.mean(
                np.load(
                    savepath
                    + "mean_{}_power_filter_bank_{}_csp_{}_{}{}.npy".format(
                        metric,
                        len(filter_banks[5]),
                        filter_banks[5][0][0],
                        filter_banks[5][-1][1],
                        clm_str_tr,
                    )
                ),
                axis=(2, 3),
            ),
        )
        results_std.append(
            np.load(
                savepath
                + "std_{}_power_filter_bank_{}_csp_{}_{}{}.npy".format(
                    metric,
                    len(filter_banks[5]),
                    filter_banks[5][0][0],
                    filter_banks[5][-1][1],
                    clm_str_tr,
                )
            ),
        )
        perm_data.append(
            np.mean(
                np.load(
                    savepath
                    + "mean_{}_power_filter_bank_{}_csp_{}_{}{}.npy".format(
                        metric,
                        len(filter_banks[5]),
                        filter_banks[5][0][0],
                        filter_banks[5][-1][1],
                        clm_str_tr,
                    )
                ),
                axis=-1,
            ),
        )

    # All Riemannian results.
    if "conv_riemann" in res_to_vis:
        label_strs.append("Convolution & TGSP")
        colors.append("royalblue")
        results.append(
            np.mean(
                np.load(
                    savepath
                    + "mean_{}_power_beta_band_riemann{}_conv_waves{}.npy".format(
                        metric, fooof_save_str, clm_str_tr
                    )
                ),
                axis=(2, 3),
            ),
        )
        results_std.append(
            np.load(
                savepath
                + "std_{}_power_beta_band_riemann{}_conv_waves{}.npy".format(
                    metric, fooof_save_str, clm_str_tr
                )
            ),
        )
        perm_data.append(
            np.mean(
                np.load(
                    savepath
                    + "mean_{}_power_beta_band_riemann{}_conv_waves{}.npy".format(
                        metric, fooof_save_str, clm_str_tr
                    )
                ),
                axis=-1,
            ),
        )
    
    if "fb_beta_riemann" in res_to_vis:
        label_strs.append(
            "Filter bank & MDM ({}-{} Hz; {} bands; {} Hz step)".format(
                filter_banks[1][0][0],
                filter_banks[1][-1][1],
                len(filter_banks[1]),
                filter_banks[1][0][1] - filter_banks[1][0][0],
            ),
        )
        colors.append("limegreen")
        results.append(
            np.mean(
                np.load(
                    savepath
                    + "mean_{}_power_filter_bank_{}_riemann_{}_{}{}.npy".format(
                        metric,
                        len(filter_banks[1]),
                        filter_banks[1][0][0],
                        filter_banks[1][-1][1],
                        clm_str_tr,
                    )
                ),
                axis=(2, 3),
            ),
        )
        results_std.append(
            np.load(
                savepath
                + "std_{}_power_filter_bank_{}_riemann_{}_{}{}.npy".format(
                    metric,
                    len(filter_banks[1]),
                    filter_banks[1][0][0],
                    filter_banks[1][-1][1],
                    clm_str_tr,
                )
            ),
        )
        perm_data.append(
            np.mean(
                np.load(
                    savepath
                    + "mean_{}_power_filter_bank_{}_riemann_{}_{}{}.npy".format(
                        metric,
                        len(filter_banks[1]),
                        filter_banks[1][0][0],
                        filter_banks[1][-1][1],
                        clm_str_tr,
                    )
                ),
                axis=-1,
            ),
        )

    if "fb_mubeta_riemann" in res_to_vis:
        label_strs.append(
            "Filter bank & MDM ({}-{} Hz; {} bands; {} Hz step)".format(
                filter_banks[2][0][0],
                filter_banks[2][-1][1],
                len(filter_banks[2]),
                filter_banks[2][0][1] - filter_banks[2][0][0],
            ),
        )
        colors.append("teal")
        results.append(
            np.mean(
                np.load(
                    savepath
                    + "mean_{}_power_filter_bank_{}_riemann_{}_{}{}.npy".format(
                        metric,
                        len(filter_banks[2]),
                        filter_banks[2][0][0],
                        filter_banks[2][-1][1],
                        clm_str_tr,
                    )
                ),
                axis=(2, 3),
            ),
        )
        results_std.append(
            np.load(
                savepath
                + "std_{}_power_filter_bank_{}_riemann_{}_{}{}.npy".format(
                    metric,
                    len(filter_banks[2]),
                    filter_banks[2][0][0],
                    filter_banks[2][-1][1],
                    clm_str_tr,
                )
            ),
        )
        perm_data.append(
            np.mean(
                np.load(
                    savepath
                    + "mean_{}_power_filter_bank_{}_riemann_{}_{}{}.npy".format(
                        metric,
                        len(filter_banks[2]),
                        filter_banks[2][0][0],
                        filter_banks[2][-1][1],
                        clm_str_tr,
                    )
                ),
                axis=-1,
            ),
        )
    
    if "fb_1_beta_riemann" in res_to_vis:
        label_strs.append(
            "Filter bank & MDM ({}-{} Hz; {} band)".format(
                filter_banks[3][0][0],
                filter_banks[3][-1][1],
                len(filter_banks[3]),
            ),
        )
        colors.append("limegreen")
        results.append(
            np.mean(
                np.load(
                    savepath
                    + "mean_{}_power_filter_bank_1_riemann_{}_{}{}.npy".format(
                        metric, filter_banks[4][0][0], filter_banks[4][-1][1], clm_str_tr
                    )
                ),
                axis=(2, 3),
            ),
        )
        results_std.append(
            np.load(
                savepath
                + "std_{}_power_filter_bank_1_riemann_{}_{}{}.npy".format(
                    metric, filter_banks[4][0][0], filter_banks[4][-1][1], clm_str_tr
                )
            ),
        )
        perm_data.append(
            np.mean(
                np.load(
                    savepath
                    + "mean_{}_power_filter_bank_1_riemann_{}_{}{}.npy".format(
                        metric, filter_banks[4][0][0], filter_banks[4][-1][1], clm_str_tr
                    )
                ),
                axis=-1,
            ),
        )

    if "fb_1_mubeta_riemann" in res_to_vis:
        label_strs.append(
            "Filter bank & MDM ({}-{} Hz; {} band)".format(
                filter_banks[5][0][0],
                filter_banks[5][-1][1],
                len(filter_banks[5]),
            ),
        )
        colors.append("teal")
        results.append(
            np.mean(
                np.load(
                    savepath
                    + "mean_{}_power_filter_bank_1_riemann_{}_{}{}.npy".format(
                        metric, filter_banks[5][0][0], filter_banks[5][-1][1], clm_str_tr
                    )
                ),
                axis=(2, 3),
            ),
        )
        results_std.append(
            np.load(
                savepath
                + "std_{}_power_filter_bank_riemann_1_{}_{}{}.npy".format(
                    metric, filter_banks[5][0][0], filter_banks[5][-1][1], clm_str_tr
                )
            ),
        )
        perm_data.append(
            np.mean(
                np.load(
                    savepath
                    + "mean_{}_power_filter_bank_1_riemann_{}_{}{}.npy".format(
                        metric, filter_banks[5][0][0], filter_banks[5][-1][1], clm_str_tr
                    )
                ),
                axis=-1,
            ),
        )
    
    # All sliding window results.
    if "conv_sliding" in res_to_vis:
        label_strs.append("Convolution & CSP (slw)")
        colors.append("orangered")
        results.append(
            np.mean(
                np.load(
                    savepath
                    + "mean_{}_power_beta_band_csp{}_conv_waves{}.npy".format(
                        metric, fooof_save_str, clm_str_sl
                    )
                ),
                axis=(2, 3),
            ),
        )
        results_std.append(
            np.load(
                savepath
                + "std_{}_power_beta_band_csp{}_conv_waves{}.npy".format(
                    metric, fooof_save_str, clm_str_sl
                )
            ),
        )
        perm_data.append(
            np.mean(
                np.load(
                    savepath
                    + "mean_{}_power_beta_band_csp{}_conv_waves{}.npy".format(
                        metric, fooof_save_str, clm_str_sl
                    )
                ),
                axis=-1,
            ),
        )
    
    if "fb_beta_sliding" in res_to_vis:
        label_strs.append(
            "Filter bank & CSP ({}-{} Hz; {} bands; {} Hz step / slw)".format(
                filter_banks[1][0][0],
                filter_banks[1][-1][1],
                len(filter_banks[1]),
                filter_banks[1][0][1] - filter_banks[1][0][0],
            ),
        )
        colors.append("goldenrod")
        results.append(
            np.mean(
                np.load(
                    savepath
                    + "mean_{}_power_filter_bank_{}_csp_{}_{}{}.npy".format(
                        metric,
                        len(filter_banks[1]),
                        filter_banks[1][0][0],
                        filter_banks[1][-1][1],
                        clm_str_sl,
                    )
                ),
                axis=(2, 3),
            ),
        )
        results_std.append(
            np.load(
                savepath
                + "std_{}_power_filter_bank_{}_csp_{}_{}{}.npy".format(
                    metric,
                    len(filter_banks[1]),
                    filter_banks[1][0][0],
                    filter_banks[1][-1][1],
                    clm_str_sl,
                )
            ),
        )
        perm_data.append(
            np.mean(
                np.load(
                    savepath
                    + "mean_{}_power_filter_bank_{}_csp_{}_{}{}.npy".format(
                        metric,
                        len(filter_banks[1]),
                        filter_banks[1][0][0],
                        filter_banks[1][-1][1],
                        clm_str_sl,
                    )
                ),
                axis=-1,
            ),
        )
    
    if "fb_mu_sliding" in res_to_vis:
        label_strs.append(
            "Filter bank & CSP ({}-{} Hz; {} bands; {} Hz step / slw)".format(
                filter_banks[0][0][0],
                filter_banks[0][-1][1],
                len(filter_banks[0]),
                filter_banks[0][0][1] - filter_banks[0][0][0],
            ),
        )
        colors.append("mediumturquoise")
        results.append(
            np.mean(
                np.load(
                    savepath
                    + "mean_{}_power_filter_bank_{}_csp_{}_{}{}.npy".format(
                        metric,
                        len(filter_banks[0]),
                        filter_banks[0][0][0],
                        filter_banks[0][-1][1],
                        clm_str_sl,
                    )
                ),
                axis=(2, 3),
            ),
        )
        results_std.append(
            np.load(
                savepath
                + "std_{}_power_filter_bank_{}_csp_{}_{}{}.npy".format(
                    metric,
                    len(filter_banks[0]),
                    filter_banks[0][0][0],
                    filter_banks[0][-1][1],
                    clm_str_sl,
                )
            ),
        )
        perm_data.append(
            np.mean(
                np.load(
                    savepath
                    + "mean_{}_power_filter_bank_{}_csp_{}_{}{}.npy".format(
                        metric,
                        len(filter_banks[0]),
                        filter_banks[0][0][0],
                        filter_banks[0][-1][1],
                        clm_str_sl,
                    )
                ),
                axis=-1,
            ),
        )
    
    if "fb_mubeta_sliding" in res_to_vis:
        label_strs.append(
            "Filter bank & CSP ({}-{} Hz; {} bands; {} Hz step / slw)".format(
                filter_banks[2][0][0],
                filter_banks[2][-1][1],
                len(filter_banks[2]),
                filter_banks[2][0][1] - filter_banks[2][0][0],
            ),
        )
        colors.append("mediumorchid")
        results.append(
            np.mean(
                np.load(
                    savepath
                    + "mean_{}_power_filter_bank_{}_csp_{}_{}{}.npy".format(
                        metric,
                        len(filter_banks[2]),
                        filter_banks[2][0][0],
                        filter_banks[2][-1][1],
                        clm_str_sl,
                    )
                ),
                axis=(2, 3),
            ),
        )
        results_std.append(
            np.load(
                savepath
                + "std_{}_power_filter_bank_{}_csp_{}_{}{}.npy".format(
                    metric,
                    len(filter_banks[2]),
                    filter_banks[2][0][0],
                    filter_banks[2][-1][1],
                    clm_str_sl,
                )
            ),
        )
        perm_data.append(
            np.mean(
                np.load(
                    savepath
                    + "mean_{}_power_filter_bank_{}_csp_{}_{}{}.npy".format(
                        metric,
                        len(filter_banks[2]),
                        filter_banks[2][0][0],
                        filter_banks[2][-1][1],
                        clm_str_sl,
                    )
                ),
                axis=-1,
            ),
        )
    
    if "fb_1_beta_sliding" in res_to_vis:
        label_strs.append(
            "Beta band filter & CSP ({}-{} Hz / slw)".format(
                filter_banks[4][0][0],
                filter_banks[4][0][1],
            ),
        )
        colors.append("goldenrod")
        results.append(
            np.mean(
                np.load(
                    savepath
                    + "mean_{}_power_filter_bank_{}_csp_{}_{}{}.npy".format(
                        metric,
                        len(filter_banks[4]),
                        filter_banks[4][0][0],
                        filter_banks[4][-1][1],
                        clm_str_sl,
                    )
                ),
                axis=(2, 3),
            ),
        )
        results_std.append(
            np.load(
                savepath
                + "std_{}_power_filter_bank_{}_csp_{}_{}{}.npy".format(
                    metric,
                    len(filter_banks[4]),
                    filter_banks[4][0][0],
                    filter_banks[4][-1][1],
                    clm_str_sl,
                )
            ),
        )
        perm_data.append(
            np.mean(
                np.load(
                    savepath
                    + "mean_{}_power_filter_bank_{}_csp_{}_{}{}.npy".format(
                        metric,
                        len(filter_banks[4]),
                        filter_banks[4][0][0],
                        filter_banks[4][-1][1],
                        clm_str_sl,
                    )
                ),
                axis=-1,
            ),
        )
    
    if "fb_1_mu_sliding" in res_to_vis:
        label_strs.append(
            "Mu band filter & CSP ({}-{} Hz / slw)".format(
                filter_banks[3][0][0],
                filter_banks[3][0][1],
            ),
        )
        colors.append("mediumturquoise")
        results.append(
            np.mean(
                np.load(
                    savepath
                    + "mean_{}_power_filter_bank_{}_csp_{}_{}{}.npy".format(
                        metric,
                        len(filter_banks[3]),
                        filter_banks[3][0][0],
                        filter_banks[3][-1][1],
                        clm_str_sl,
                    )
                ),
                axis=(2, 3),
            ),
        )
        results_std.append(
            np.load(
                savepath
                + "std_{}_power_filter_bank_{}_csp_{}_{}{}.npy".format(
                    metric,
                    len(filter_banks[3]),
                    filter_banks[3][0][0],
                    filter_banks[3][-1][1],
                    clm_str_sl,
                )
            ),
        )
        perm_data.append(
            np.mean(
                np.load(
                    savepath
                    + "mean_{}_power_filter_bank_{}_csp_{}_{}{}.npy".format(
                        metric,
                        len(filter_banks[3]),
                        filter_banks[3][0][0],
                        filter_banks[3][-1][1],
                        clm_str_sl,
                    )
                ),
                axis=-1,
            ),
        )
    
    if "fb_1_mubeta_sliding" in res_to_vis:
        label_strs.append(
            "Mu-beta band filter & CSP ({}-{} Hz / slw)".format(
                filter_banks[5][0][0],
                filter_banks[5][0][1],
            ),
        )
        colors.append("mediumorchid")
        results.append(
            np.mean(
                np.load(
                    savepath
                    + "mean_{}_power_filter_bank_{}_csp_{}_{}{}.npy".format(
                        metric,
                        len(filter_banks[5]),
                        filter_banks[5][0][0],
                        filter_banks[5][-1][1],
                        clm_str_sl,
                    )
                ),
                axis=(2, 3),
            ),
        )
        results_std.append(
            np.load(
                savepath
                + "std_{}_power_filter_bank_{}_csp_{}_{}{}.npy".format(
                    metric,
                    len(filter_banks[5]),
                    filter_banks[5][0][0],
                    filter_banks[5][-1][1],
                    clm_str_sl,
                )
            ),
        )
        perm_data.append(
            np.mean(
                np.load(
                    savepath
                    + "mean_{}_power_filter_bank_{}_csp_{}_{}{}.npy".format(
                        metric,
                        len(filter_banks[5]),
                        filter_banks[5][0][0],
                        filter_banks[5][-1][1],
                        clm_str_sl,
                    )
                ),
                axis=-1,
            ),
        )


    # ----- #
    # Figures' hyper-parameters.

    # Decoding levels.
    if metric == "accuracy":
        # IF USING "ACCURACY", CHANCE LEVEL COULD BE ADDED
        radii = np.arange(40, 101, 10)
    elif metric == "rocauc":
        radii = np.arange(0.4, 1.01, 0.2)

    # Time points.
    time_radii_aug = 2.0
    time_radii = np.arange(
        time_points[0],
        time_points[-1] + 0.1 + time_radii_aug,
        time_points[2] - time_points[0],
    )
    if time_radii[-1] - time_radii_aug < time_points[-1]:
        time_radii = np.concatenate(
            [time_radii, [np.ceil(time_points[-1]) + time_radii_aug]]
        )

    # Positioning and width of bars.
    center = 2 * np.pi / len(label_strs)
    width = center - (0.5 / len(label_strs))
    ids = np.arange(0, len(label_strs), 1)
    angles = []
    for id in ids:
        angles.append((np.pi / 2) - center * id)


    # ----- #
    # Plots.

    # Across-subjects average temporal decoding.
    gs00 = gs0[d, 0].subgridspec(1, 1)
    ax00 = fig0.add_subplot(gs00[0])
    gs01 = gs0[d, 1].subgridspec(1, 1)
    ax01 = fig0.add_subplot(gs01[0])
    gs02 = gs0[d, 2].subgridspec(1, 1)
    ax02 = fig0.add_subplot(gs02[0])
    gs03 = gs0[d, 3].subgridspec(1, 1)
    ax03 = fig0.add_subplot(gs03[0])

    # Decoding score in specific time points
    # and time point to reach 90% of max decoding.
    gs10 = gs1[d + 1, 0].subgridspec(1, 1)
    ax10 = fig1.add_subplot(gs10[0], projection="polar")
    gs11 = gs1[d + 1, 1].subgridspec(1, 1)
    ax11 = fig1.add_subplot(gs11[0], projection="polar")
    gs12 = gs1[d + 1, 2].subgridspec(1, 1)
    ax12 = fig1.add_subplot(gs12[0], projection="polar")
    gs13 = gs1[d + 1, 3].subgridspec(1, 1)
    ax13 = fig1.add_subplot(gs13[0], projection="polar")
    gs14 = gs1[d + 1, 4].subgridspec(1, 1)
    ax14 = fig1.add_subplot(gs14[0], projection="polar")
    gs15 = gs1[d + 1, 5].subgridspec(1, 1)
    ax15 = fig1.add_subplot(gs15[0], projection="polar")

    # ----- #
    # Permutation cluster tests.
    if (
        classification_mode == "time_resolved"
        and perm == True
        and "conv" in res_to_vis
        and len(res_to_vis) >= 2
    ):
        perm_cl_test = vis_permutation_cluster_test(
            perm_data,
            res_to_vis,
            colors,
            None,
        )
    elif (
        classification_mode == "both"
        and perm == True
        and "conv" in res_to_vis
        and "conv_sliding" in res_to_vis
    ):
        perm_cl_test = vis_permutation_cluster_test(
            perm_data[:sl_res_id],
            res_to_vis[:sl_res_id],
            colors[:sl_res_id],
            None,
        )
        perm_cl_test_sl = vis_permutation_cluster_test(
            perm_data[sl_res_id:],
            res_to_vis[sl_res_id:],
            colors[sl_res_id:],
            None,
        )
    perm_offset_0 = 0.02
    perm_offset_1 = len(perm_cl_test) / 100 + 0.01

    # ----- #
    # Fig 0: Time-resolved avergaged results.
    max_dec_times = []
    for r, (result, color, label_str) in enumerate(zip(results, colors, label_strs)):
        # Retrieve mean accuracy and std.
        across_subjects = np.mean(result, axis=0)
        across_subjects_std = np.std(result, axis=0) / np.sqrt(result.shape[0])

        # Find time point with max decoding score.
        if (
            classification_mode == "time_resolved"
            or (classification_mode == "both" and r < sl_res_id)
        ):
            max_dec_times.append(
                [
                    np.max(across_subjects[time_ids[0] : time_ids[-1] + 1]),
                    time[np.argmax(across_subjects[time_ids[0] : time_ids[-1] + 1])],
                ]
            )
        elif classification_mode == "both" and r >= sl_res_id:
            max_dec_times.append(
                [
                    np.max(across_subjects[time_ids_sl[0] : time_ids_sl[-1] + 1]),
                    time[np.argmax(across_subjects[time_ids_sl[0] : time_ids_sl[-1] + 1])],
                ]
            )


        # ----- #
        # Plot across-subjects average accuracy.
        if label_str == "Convolution & CSP":
            # Time-resolved decoding accuracy.
            if d == 0:
                ax00.plot(
                    time,
                    across_subjects,
                    c=color,
                    linewidth=linewidths[0],
                    label=label_str,
                )
                ax01.plot(time, across_subjects, c=color, linewidth=linewidths[0])
                if classification_mode == "time_resolved":
                    ax02.plot(time, across_subjects, c=color, linewidth=linewidths[0])
                    ax03.plot(time, across_subjects, c=color, linewidth=linewidths[0])
            else:
                ax00.plot(time, across_subjects, c=color, linewidth=linewidths[0])
                ax01.plot(time, across_subjects, c=color, linewidth=linewidths[0])
                if classification_mode == "time_resolved":
                    ax02.plot(time, across_subjects, c=color, linewidth=linewidths[0])
                    ax03.plot(time, across_subjects, c=color, linewidth=linewidths[0])

            # Across-subjects std.
            bag_of_axes = [ax00, ax01, ax02] if classification_mode == "time_resolved" else [ax00, ax01]
            for ax0x in bag_of_axes:
                ax0x.fill_between(
                    time,
                    across_subjects - across_subjects_std,
                    across_subjects + across_subjects_std,
                    color=color,
                    alpha=0.2,
                )
        
        # Simple filters.
        if (
            label_str == "Beta band filter & CSP (15-30 Hz)"
            or label_str == "Mu band filter & CSP (6-15 Hz)"
            or label_str == "Mu-beta band filter & CSP (6-30 Hz)"
        ):
            # Time-resolved decoding accuracy.
            if d == 0:
                ax00.plot(
                    time,
                    across_subjects,
                    c=color,
                    linewidth=linewidths[0],
                    label=label_str,
                )
            else:
                ax00.plot(time, across_subjects, c=color, linewidth=linewidths[0])

            # Plot across-subjects std.
            ax00.fill_between(
                time,
                across_subjects - across_subjects_std,
                across_subjects + across_subjects_std,
                color=color,
                alpha=0.2,
            )

            # Permutation cluster test visualization.
            pipe_ids = np.where(np.array(label_strs) == label_str)[0]
            for pid in pipe_ids:
                dec_lines, dec_col = perm_cl_test[pid]
                ax00.plot(
                    time[time_ids[0] : time_ids[-1] + 1],
                    dec_lines[0][time_ids[0] : time_ids[-1] + 1] - perm_offset_1,
                    c=dec_col[0],
                    linewidth=linewidths[2],
                )
                ax00.plot(
                    time[time_ids[0] : time_ids[-1] + 1],
                    dec_lines[1][time_ids[0] : time_ids[-1] + 1] - perm_offset_1,
                    c=dec_col[1],
                    linewidth=linewidths[2],
                )

        # Filter banks.
        if (
            label_str == "Filter bank & CSP (15-30 Hz; 5 bands; 3 Hz step)"
            or label_str == "Filter bank & CSP (6-15 Hz; 3 bands; 3 Hz step)"
            or label_str == "Filter bank & CSP (6-30 Hz; 8 bands; 3 Hz step)"
        ):
            # Time-resolved decoding accuracy.
            ax01.plot(time, across_subjects, c=color, linewidth=linewidths[0])

            # Across-subjects std.
            ax01.fill_between(
                time,
                across_subjects - across_subjects_std,
                across_subjects + across_subjects_std,
                color=color,
                alpha=0.2,
            )

            # Permutation cluster test visualization.
            pipe_ids = np.where(np.array(label_strs) == label_str)[0]
            for pid in pipe_ids:
                dec_lines, dec_col = perm_cl_test[pid]
                ax01.plot(
                    time[time_ids[0] : time_ids[-1] + 1],
                    dec_lines[0][time_ids[0] : time_ids[-1] + 1] - perm_offset_0,
                    c=dec_col[0],
                    linewidth=linewidths[2],
                )
                ax01.plot(
                    time[time_ids[0] : time_ids[-1] + 1],
                    dec_lines[1][time_ids[0] : time_ids[-1] + 1] - perm_offset_0,
                    c=dec_col[1],
                    linewidth=linewidths[2],
                )

        # Riemannian.
        if label_str == "Convolution & TGSP":
            # Time-resolved decoding accuracy.
            if d == 0:
                ax02.plot(
                    time,
                    across_subjects,
                    c=color,
                    linewidth=linewidths[0],
                    label=label_str,
                )
            else:
                ax02.plot(time, across_subjects, c=color, linewidth=linewidths[0])

            # Plot across-subjects std.
            ax02.fill_between(
                time,
                across_subjects - across_subjects_std,
                across_subjects + across_subjects_std,
                color=color,
                alpha=0.2,
            )

            # Permutation cluster test visualization.
            pipe_ids = np.where(np.array(label_strs) == label_str)[0]
            for pid in pipe_ids:
                dec_lines, dec_col = perm_cl_test[pid]
                ax02.plot(
                    time[time_ids[0] : time_ids[-1] + 1],
                    dec_lines[0][time_ids[0] : time_ids[-1] + 1] - perm_offset_0,
                    c=dec_col[0],
                    linewidth=linewidths[2],
                )
                ax02.plot(
                    time[time_ids[0] : time_ids[-1] + 1],
                    dec_lines[1][time_ids[0] : time_ids[-1] + 1] - perm_offset_0,
                    c=dec_col[1],
                    linewidth=linewidths[2],
                )

        # Sliding window convolution decoding.
        if label_str == "Convolution & CSP (slw)":
            ax02.plot(time_sl, across_subjects, c=color, linewidth=linewidths[0])
            ax03.plot(time_sl, across_subjects, c=color, linewidth=linewidths[0])

            bag_of_axes = [ax02, ax03]
            for ax0x in bag_of_axes:
                ax0x.fill_between(
                    time_sl,
                    across_subjects - across_subjects_std,
                    across_subjects + across_subjects_std,
                    color=color,
                    alpha=0.2,
                )

        # Sliding window filtering.
        if (
            label_str == "Beta band filter & CSP (15-30 Hz / slw)"
            or label_str == "Mu band filter & CSP (6-15 Hz / slw)"
            or label_str == "Mu-beta band filter & CSP (6-30 Hz / slw)"
        ):
            # Time-resolved decoding accuracy.
            ax02.plot(time_sl, across_subjects, c=color, linewidth=linewidths[0])

            # Plot across-subjects std.
            ax02.fill_between(
                time_sl,
                across_subjects - across_subjects_std,
                across_subjects + across_subjects_std,
                color=color,
                alpha=0.2,
            )

            # Permutation cluster test visualization.
            pipe_ids = np.where(np.array(label_strs[sl_res_id:]) == label_str)[0]
            for pid in pipe_ids:
                dec_lines, dec_col = perm_cl_test_sl[pid]
                ax02.plot(
                    time_sl[time_ids_sl[0] : time_ids_sl[-1] + 1],
                    dec_lines[0][time_ids_sl[0] : time_ids_sl[-1] + 1] - perm_offset_1,
                    c=dec_col[0],
                    linewidth=linewidths[2],
                )
                ax02.plot(
                    time_sl[time_ids_sl[0] : time_ids_sl[-1] + 1],
                    dec_lines[1][time_ids_sl[0] : time_ids_sl[-1] + 1] - perm_offset_1,
                    c=dec_col[1],
                    linewidth=linewidths[2],
                )

        # Sliding window filter bank.
        if (
            label_str == "Filter bank & CSP (15-30 Hz; 5 bands; 3 Hz step / slw)"
            or label_str == "Filter bank & CSP (6-15 Hz; 3 bands; 3 Hz step / slw)"
            or label_str == "Filter bank & CSP (6-30 Hz; 8 bands; 3 Hz step / slw)"
        ):
            # Time-resolved decoding accuracy.
            ax03.plot(time_sl, across_subjects, c=color, linewidth=linewidths[0])

            # Plot across-subjects std.
            ax03.fill_between(
                time_sl,
                across_subjects - across_subjects_std,
                across_subjects + across_subjects_std,
                color=color,
                alpha=0.2,
            )

            # Permutation cluster test visualization.
            pipe_ids = np.where(np.array(label_strs[sl_res_id:]) == label_str)[0]
            for pid in pipe_ids:
                dec_lines, dec_col = perm_cl_test_sl[pid]
                ax03.plot(
                    time_sl[time_ids_sl[0] : time_ids_sl[-1] + 1],
                    dec_lines[0][time_ids_sl[0] : time_ids_sl[-1] + 1] - perm_offset_0,
                    c=dec_col[0],
                    linewidth=linewidths[2],
                )
                ax03.plot(
                    time_sl[time_ids_sl[0] : time_ids_sl[-1] + 1],
                    dec_lines[1][time_ids_sl[0] : time_ids_sl[-1] + 1] - perm_offset_0,
                    c=dec_col[1],
                    linewidth=linewidths[2],
                )


        # ----- #
        # Fig 1: Across-subjects average decoding in specific time points
        # and time to reach 90% of max decoding.
        for t, (tp, ax1x) in enumerate(zip(time_ids, [ax10, ax11, ax12, ax13, ax14])):
            # Fake bar fixes radii aliasing when the rest of the bars
            # do not have values close to 1.0
            ax1x.bar(
                x=angles[0],
                height=1,
                width=width,
                bottom=0,
                color="w",
                alpha=0.0,
                linewidth=tick_size,
            )

            # Decoding score around this time point (+/- 1 time window).
            if (
                classification_mode == "time_resolved"
                or (classification_mode == "both" and r < sl_res_id)
            ):
                dec_score_tp = np.mean(across_subjects[tp - 1 : tp + 2])
                dec_score_std_tp = np.mean(across_subjects_std[tp - 1 : tp + 2])
            elif classification_mode == "both" and r >= sl_res_id:
                dec_score_tp = np.mean(across_subjects[time_ids_sl[t] - 1 : time_ids_sl[t] + 2])
                dec_score_std_tp = np.mean(across_subjects_std[time_ids_sl[t] - 1 : time_ids_sl[t] + 2])
            

            ax1x.bar(
                x=angles[r],
                height=dec_score_tp,
                yerr=dec_score_std_tp,
                width=width,
                bottom=0,
                color=color,
                linewidth=tick_size,
            )

            # Set minimum visible decoding level.
            ax1x.bar(x=0, height=radii[0], width=2 * np.pi, bottom=0, color="w")

            # Accuracy text.
            ax1x.text(
                x=angles[r],
                y=radii[-1],
                s="{0:.2f}".format(dec_score_tp),
                fontsize=fontsizes[2],
            )

            # Remove spines, ticks, ticklabels.
            ax1x.spines[["polar"]].set_visible(False)
            ax1x.set_rgrids(radii, fontweight=tick_size)
            ax1x.yaxis.grid(linewidth=1.0)
            ax1x.set_xticks([])
            ax1x.set_yticklabels([])

            # Time point text.
            if r == 0:
                ax1x.text(
                    x=angles[0] - 1,
                    y=0,
                    s="t={0:.2f} s".format(time_points[t]),
                    fontweight="bold",
                    fontsize=fontsizes[2],
                )

        # Titles.
        if r == 0:
            ax10.set_title(
                title_str,
                fontsize=fontsizes[1],
                loc="left",
                fontweight="bold",
            )

    # Fake bar fixes radii aliasing when the rest of the bars
    # do not have values close to last time point.
    ax15.bar(
        x=angles[0],
        height=time_radii[-1],
        width=width,
        bottom=0,
        color="w",
        alpha=0.0,
        linewidth=tick_size,
    )

    # Fig 1: Across-subjects average decoding in specific time points
    # and time to reach 90% of max decoding.
    ax15.bar(
        x=angles[0],
        height=time_points[-1],
        width=width,
        bottom=time_radii_aug,
        color="w",
        alpha=0.0,
        linewidth=tick_size,
    )

    # Maximum decoding score across all pipelines.
    max_dec_score = np.max(np.array(max_dec_times)[:, 0])

    for r, (result, color, label_str, max_dec_time) in enumerate(
        zip(results, colors, label_strs, max_dec_times)
    ):
        # Retrieve mean accuracy.
        across_subjects = np.mean(result, axis=0)

        # Select appropriate time point.
        add_txt = True
        if (
            classification_mode == "time_resolved"
            or (classification_mode == "both" and r < sl_res_id)
        ):
            try:
                tp90 = time[time_ids[0] : time_ids[-1] + 1][
                    np.where(
                        np.around(across_subjects[time_ids[0] : time_ids[-1] + 1], decimals=2)
                        >= np.around(0.9 * max_dec_score, decimals=2)
                    )[0]
                ][0]
                add_txt = True
            except:
                tp90 = time_points[-1]
                add_txt = False
        elif classification_mode == "both" and r >= sl_res_id:
            try:
                tp90 = time_sl[time_ids_sl[0] : time_ids_sl[-1] + 1][
                    np.where(
                        np.around(across_subjects[time_ids_sl[0] : time_ids_sl[-1] + 1], decimals=2)
                        >= np.around(0.9 * max_dec_score, decimals=2)
                    )[0]
                ][0]
                add_txt = True
            except:
                tp90 = time_points[-1]
                add_txt = False

        # Time bar.
        ax15.bar(
            x=angles[r],
            height=tp90,
            width=width,
            bottom=time_radii_aug,
            color=color,
            label=label_str if d == 0 else "",
            linewidth=tick_size,
        )
        if add_txt == False:
            ax15.bar(
                x=angles[r],
                height=tp90,
                width=width,
                bottom=time_radii_aug,
                color="gainsboro",
                linewidth=tick_size,
            )

        # Set minimum visible time.
        ax15.bar(
            x=angles[0], height=time_radii_aug, width=2 * np.pi, bottom=0, color="w"
        )

        # Time text.
        if add_txt == True:
            ax15.text(
                x=angles[r],
                y=time_radii[-1],
                s="{0:.1f}s".format(tp90),
                fontsize=fontsizes[2],
            )

    # Remove spines, ticks, ticklabels.
    ax15.spines[["polar"]].set_visible(False)
    ax15.set_rgrids(
        time_radii[np.where(time_radii >= time_radii_aug)], fontweight=tick_size
    )
    ax15.yaxis.grid(linewidth=1.0)
    ax15.set_xticks([])
    ax15.set_yticklabels([])

    # Title.
    if d == 0:
        ax15.set_title(
            "time needed to reach 90%\nof max decoding score\n\n({})".format(
                np.around(0.9 * max_dec_score, decimals=2)
            ),
            fontsize=fontsizes[2],
            loc="left",
            fontweight="bold",
        )
    else:
        ax15.set_title(
            "({})".format(np.around(0.9 * max_dec_score, decimals=2)),
            fontsize=fontsizes[2],
            loc="left",
            fontweight="bold",
        )


    # ----- #
    # Fig 0 adjustments.

    # Title and ylabel.
    ax00.set_ylabel("Decoding score", fontsize=fontsizes[0])
    ax00.set_title(
        title_str,
        fontsize=fontsizes[1],
        loc="left",
        fontweight="bold",
    )

    for ax0x in [ax00, ax01, ax02, ax03]:
        # Chance level.
        ax0x.hlines(
            0.5,
            time[0],
            time[-1],
            linestyles="dashed",
            colors="grey",
            linewidth=linewidths[1],
        )

        # Trial beginning and end.
        ax0x.vlines(
            (exp_time_periods[1], exp_time_periods[2]),
            0.4,
            1.0,
            linestyles="dotted",
            colors="k",
            linewidth=linewidths[1],
        )

        # Y axis limits and ticks.
        ax0x.set_ylim([0.4, 1.09])
        ax0x.set_yticks(np.arange(0.4, 1.01, 0.1))

        # X axis limits and ticks.
        ax0x.set_xticks(np.arange(0.0, time[-1], 1.0))

        # Axes labels.
        if d == len(datas) - 1:
            ax0x.set_xlabel("Time (s)", fontsize=fontsizes[1])

        # Axes tick size.
        ax0x.tick_params(axis="both", labelsize=fontsizes[1])

        # Spines.
        ax0x.spines[["top", "right"]].set_visible(False)


# ----- #
# Legends.
leg_cols = 3 # len(res_to_vis) // 2 + len(res_to_vis) % 2
fig0.legend(
    frameon=False,
    title="Classification pipeline",
    alignment="left",
    fontsize=fontsizes[1],
    title_fontsize=fontsizes[0],
    ncols=leg_cols,
)
fig1.legend(
    frameon=False,
    title="Classification pipeline",
    alignment="left",
    fontsize=fontsizes[1],
    title_fontsize=fontsizes[0],
    ncols=leg_cols,
)


# ----- #
# Optional saving.
if savefigs == True:
    fig0_name = savepath + "dataset_average_time_resolved{}_decoding_scores{}.{}".format(
        clm_str_tr, fooof_save_str, plot_format
    )
    fig1_name = savepath + "dataset_average_time_points{}_decoding_scores{}.{}".format(
        clm_str_tr, fooof_save_str, plot_format
    )
    fig0.savefig(fig0_name, dpi=dpi, facecolor="w", edgecolor="w")
    fig1.savefig(fig1_name, dpi=dpi, facecolor="w", edgecolor="w")
else:
    plt.show()
