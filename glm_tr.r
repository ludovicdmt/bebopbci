library("lme4")
library("lmtest")
library("phia")
library("car")
library("emmeans")

# Model 1.
time_res_class_res_tp1 <- read.csv("~/Codes/bebop_test/time_res_class_res_nfs_tp1.csv", header=TRUE, stringsAsFactors=FALSE)

time_res_class_res_tp1$Feature<-as.factor(time_res_class_res_tp1$Feature)
time_res_class_res_tp1$Dataset<-as.factor(time_res_class_res_tp1$Dataset)
time_res_class_res_tp1$Subject<-as.factor(time_res_class_res_tp1$Subject)

model_tp1 <- glmer(Accuracy ~ Feature + (1|Dataset/Subject), data=time_res_class_res_tp1, family=binomial(), weights=time_res_class_res_tp1$Trials)

summary(model_tp1)
Anova(model_tp1)
emmeans(model_tp1, pairwise ~ Feature)

# Model 2.
time_res_class_res_tp2 <- read.csv("~/Codes/bebop_test/time_res_class_res_nfs_tp2.csv", header=TRUE, stringsAsFactors=FALSE)

time_res_class_res_tp2$Feature<-as.factor(time_res_class_res_tp2$Feature)
time_res_class_res_tp2$Dataset<-as.factor(time_res_class_res_tp2$Dataset)
time_res_class_res_tp2$Subject<-as.factor(time_res_class_res_tp2$Subject)

model_tp2 <- glmer(Accuracy ~ Feature + (1|Dataset/Subject), data=time_res_class_res_tp2, family=binomial(), weights=time_res_class_res_tp2$Trials)

summary(model_tp2)
Anova(model_tp2)
emmeans(model_tp2, pairwise ~ Feature)

# Model 3.
time_res_class_res_tp3 <- read.csv("~/Codes/bebop_test/time_res_class_res_nfs_tp3.csv", header=TRUE, stringsAsFactors=FALSE)

time_res_class_res_tp3$Feature<-as.factor(time_res_class_res_tp3$Feature)
time_res_class_res_tp3$Dataset<-as.factor(time_res_class_res_tp3$Dataset)
time_res_class_res_tp3$Subject<-as.factor(time_res_class_res_tp3$Subject)

model_tp3 <- glmer(Accuracy ~ Feature + (1|Dataset/Subject), data=time_res_class_res_tp3, family=binomial(), weights=time_res_class_res_tp3$Trials)

summary(model_tp3)
Anova(model_tp3)
emmeans(model_tp3, pairwise ~ Feature)

# Model 4.
time_res_class_res_tp4 <- read.csv("~/Codes/bebop_test/time_res_class_res_nfs_tp4.csv", header=TRUE, stringsAsFactors=FALSE)

time_res_class_res_tp4$Feature<-as.factor(time_res_class_res_tp4$Feature)
time_res_class_res_tp4$Dataset<-as.factor(time_res_class_res_tp4$Dataset)
time_res_class_res_tp4$Subject<-as.factor(time_res_class_res_tp4$Subject)

model_tp4 <- glmer(Accuracy ~ Feature + (1|Dataset/Subject), data=time_res_class_res_tp4, family=binomial(), weights=time_res_class_res_tp4$Trials)

summary(model_tp4)
Anova(model_tp4)
emmeans(model_tp4, pairwise ~ Feature)