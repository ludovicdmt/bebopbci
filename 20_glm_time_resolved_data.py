import numpy as np
import pandas as pd

from moabb.datasets import (
    Zhou2016,
    BNCI2014004,
    BNCI2014001,
    MunichMI,
    Weibo2014,
    Cho2017,
)

from preprocess import (
    load_sub,
    apply_preprocessing,
    Dreyer2023,
)
from help_funcs import load_exp_variables


# ----- #
# Hyperparameters.
metric = "rocauc"
if metric == "rocauc":
    metric_str = "score"
elif metric == "accuracy":
    metric_str = metric

remove_fooof = False
if remove_fooof == True:
    fooof_save_str = ""
elif remove_fooof == False:
    fooof_save_str = "_nfs"

classification_mode = "time_resolved"
if classification_mode == "time_resolved":
    clm_str = "_tr"
elif classification_mode == "sliding":
    clm_str = "_sl"


# ----- #
# Dataset selection.
datas = [
    "zhou2016",
    "2014004",
    "2014001",
    "weibo2014",
    # "munichmi",
    # "cho2017",
    # "dreyer2023",
]

# Mode.
mode = "local"  # "local", "cluster"
if mode == "local":
    basepath = "/home/sotpapad/Codes/"
elif mode == "cluster":
    basepath = "/mnt/data/sotiris.papadopoulos/"

#res_to_load = ["conv", "fb_beta", "fb_mu", "fb_mubeta"]
res_to_load = [
    "conv",
    "fb_beta",
    # "fb_mu",
    "fb_mubeta",
    "fb_1_beta",
    "fb_1_mubeta",
    "conv_riemann",
]

filter_banks = [
    [[6, 9], [9, 12], [12, 15]],
    [[15, 18], [18, 21], [21, 24], [24, 27], [27, 30]],
    [[6, 9], [9, 12], [12, 15], [15, 18], [18, 21], [21, 24], [24, 27], [27, 30]],
    [[15, 30]],
    [[6, 30]],
]


# ----- #
for j in range(len(res_to_load)):
    
    # ----- #
    # Data parsing.
    all_datasets = []
    all_subjects = []
    all_variables = []
    all_results = []
    all_trials = []

    for d, data in enumerate(datas):
        if data == "zhou2016":
            dataset = Zhou2016()
            variables_path = "{}zhou_2016/variables.json".format(basepath)
            title_str = "Zhou 2016"
        elif data == "2014004":
            dataset = BNCI2014004()
            variables_path = "{}2014_004/variables.json".format(basepath)
            title_str = "BNCI 2014-004"
        elif data == "2014001":
            dataset = BNCI2014001()
            variables_path = "{}2014_001/variables.json".format(basepath)
            title_str = "BNCI 2014-001"
        elif data == "munichmi":
            dataset = MunichMI()
            variables_path = "{}munichmi/variables.json".format(basepath)
            title_str = "Munich MI (Grosse-Wentrup)"
        elif data == "cho2017":
            dataset = Cho2017()
            variables_path = "{}cho_2017/variables.json".format(basepath)
            title_str = "Cho 2017"
        elif data == "weibo2014":
            dataset = Weibo2014()
            variables_path = "{}weibo_2014/variables.json".format(basepath)
            title_str = "Weibo 2014"
        elif data == "dreyer2023":
            dataset = Dreyer2023()
            variables_path = "{}dreyer_2023/variables.json".format(basepath)
            title_str = "Dreyer 2023"


        # ----- #
        # Loading of dataset-specific variables.
        experimental_vars = load_exp_variables(json_filename=variables_path)

        savepath = experimental_vars["dataset_path"]

        subs = np.arange(1, experimental_vars["n_subjects"] + 1, 1).tolist()
        if data == "cho2017":
            # Some subjects are not included in the dataset.
            subs = np.delete(np.array(subs), [31, 45, 48]).tolist()

        # Time-related variables.
        exp_time_periods = experimental_vars["exp_time_periods"]
        bin_dt = experimental_vars["bin_dt"]

        tmin = experimental_vars["tmin"]
        tmax = experimental_vars["tmax"]

        time = np.around(
            np.arange(
                exp_time_periods[1] - 0.5, exp_time_periods[2] + 0.5 + bin_dt, bin_dt
            ),
            2,
        )

        # Time points of interest.
        time_points = [0.5, 1.0, 2.0, exp_time_periods[2]]
        time_ids = [int(np.where(time == tp)[0]) for tp in time_points]

        # Dataset id.
        all_datasets.append(np.repeat(d + 1, len(subs) * len(res_to_load)))

        # Subject id.
        all_subjects.append(np.hstack(np.hstack([np.repeat(v+1, len(res_to_load)) for v in range(len(subs))])))

        # Feature id.
        all_variables.append(np.tile([v+1 for v in range(len(res_to_load))], len(subs)))

        # ----- #
        # Loading of decoding results.
        dataset_results = []
        if "conv" in res_to_load:
            dataset_results.append(
                np.mean(
                    np.load(
                        savepath
                        + "mean_{}_power_beta_band_csp{}_conv_waves{}.npy".format(
                            metric, fooof_save_str, clm_str
                        )
                    ),
                    axis=(2, 3),
                ),
            )
        
        if "conv" in res_to_load:
            dataset_results.append(
                np.mean(
                    np.load(
                        savepath
                        + "mean_{}_power_beta_band_riemann{}_conv_waves{}.npy".format(
                            metric, fooof_save_str, clm_str
                        )
                    ),
                    axis=(2, 3),
                ),
            )

        if "fb_beta" in res_to_load:
            dataset_results.append(
                np.mean(
                    np.load(
                        savepath
                        + "mean_{}_power_filter_bank_{}_csp_{}_{}{}.npy".format(
                            metric,
                            len(filter_banks[1]),
                            filter_banks[1][0][0],
                            filter_banks[1][-1][1],
                            clm_str,
                        )
                    ),
                    axis=(2, 3),
                ),
            )

        if "fb_mu" in res_to_load:
            dataset_results.append(
                np.mean(
                    np.load(
                        savepath
                        + "mean_{}_power_filter_bank_{}_csp_{}_{}{}.npy".format(
                            metric,
                            len(filter_banks[0]),
                            filter_banks[0][0][0],
                            filter_banks[0][-1][1],
                            clm_str,
                        )
                    ),
                    axis=(2, 3),
                ),
            )

        if "fb_mubeta" in res_to_load:
            dataset_results.append(
                np.mean(
                    np.load(
                        savepath
                        + "mean_{}_power_filter_bank_{}_csp_{}_{}{}.npy".format(
                            metric,
                            len(filter_banks[2]),
                            filter_banks[2][0][0],
                            filter_banks[2][-1][1],
                            clm_str,
                        )
                    ),
                    axis=(2, 3),
                ),
            )
        
        if "fb_1_beta" in res_to_load:
            dataset_results.append(
                np.mean(
                    np.load(
                        savepath
                        + "mean_{}_power_filter_bank_{}_csp_{}_{}{r.npy".format(
                            metric,
                            len(filter_banks[3]),
                            filter_banks[3][0][0],
                            filter_banks[3][-1][1],
                            clm_str,
                        )
                    ),
                    axis=(2, 3),
                ),
            )
        
        if "fb_1_mubeta" in res_to_load:
            dataset_results.append(
                np.mean(
                    np.load(
                        savepath
                        + "mean_{}_power_filter_bank_{}_csp_{}_{}{}.npy".format(
                            metric,
                            len(filter_banks[4]),
                            filter_banks[4][0][0],
                            filter_banks[4][-1][1],
                            clm_str,
                        )
                    ),
                    axis=(2, 3),
                ),
            )
        
        # All subjects' results.
        n_trials = []
        sub_results = []
        
        for s, subj in enumerate(subs):

            # ----- #
            # Epochs loading in order to find the number of trials.
            if data == "weibo2014":
                epochs, labels, meta = load_sub(
                    subj,
                    dataset,
                    tmin,
                    tmax,
                    exp_time_periods[:2],
                    savepath,
                    band_pass=[0, 90],
                )
            else:
                epochs, labels, meta = load_sub(
                    subj, dataset, tmin, tmax, exp_time_periods[:2], savepath
                )
            
            # Pre-processing.
            print("Applying pre-processing...")

            _, labels, _, _, _ = apply_preprocessing(
                epochs,
                labels,
                meta,
                channels=None,
                zapit=False,
                return_epochs=True,
            )

            n_trials.append(len(labels))

            # ----- #
            # Corresponding classification results.
            sub_results.append([np.mean(dataset_result[s, time_ids[j] - 1 : time_ids[j] + 2]) for dataset_result in dataset_results])

        all_results.append(np.hstack(sub_results))
        all_trials.append(np.tile(n_trials, len(res_to_load)))


    # ----- #
    # GLMs
    all_datasets = np.hstack(all_datasets)
    all_subjects = np.hstack(all_subjects)
    all_variables = np.hstack(all_variables)
    all_trials = np.hstack(all_trials)
    all_results = np.hstack(all_results)

    # Full model as pandas dataframe.
    lmm_data = pd.DataFrame(
        np.array(
            [
                all_datasets,
                all_subjects,
                all_variables,
                all_trials,
                all_results,
            ]
        ).T,
        columns=["Dataset", "Subject", "Feature", "Trials", "Accuracy"],
    )

    savepath = "/home/sotpapad/Codes/bebop_test/"

    lmm_data.to_csv(savepath + "time_res{}_class_res{}_tp{}.csv".format(clm_str, fooof_save_str, j+1))